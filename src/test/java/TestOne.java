import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.FirstPage;

import static helpers.testData.url;
import static pages.BasePage.makeScreenOnTestFail;
import static pages.FirstPage.*;

public class TestOne {

    public WebDriver driver;
    public pages.BasePage BasePage;
    private FirstPage newFirstPage;

    @BeforeMethod
    public void SetupClass(){
        BasePage = new BasePage();
        driver = BasePage.initialize_driver();
        newFirstPage = new FirstPage(driver);
    }


    @Test (description = "Ввод некорректного логина", priority = 1)
    public void testOpenLinkAndIncorrectLogIN(){
        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем страницу нашего сайта
        openAnyPage(driver, url);

        //Печатаем титул страницы в лог
        System.out.println("Page title is: " + driver.getTitle());

        //Вводим корректный логин
        enterField(FieldUser,"qwerty");

        //Запускаем авторизацию
        pressEnterForElement(FieldUser);

        //Ассерт того что div с результатами отображается
        checkElementDisplayed(searchResult1);

    }

    @Test (description = "Ввод корректного логина и пароля", priority = 2)
    public void testOpenLinkAndCorrectLoginAndPassword(){
        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем страницу нашего сайта
        openAnyPage(driver, url);

        //Печатаем титул страницы в лог
        System.out.println("Page title is: " + driver.getTitle());

        //Вводим корректный логин
        enterField(FieldUser,"tomsmith");

        //Вводим корректный логин
        enterField(FieldPass,"SuperSecretPassword!");

        //Запускаем авторизацию
        pressEnterForElement(FieldPass);

        //Ассерт того что div с результатами отображается
        checkElementDisplayed(searchResult2);


    }


    @Test (description = "Ввод корректного логина и некорректного пароля", priority = 3)
    public void testOpenLink3(){

        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем страницу нашего сайта
        openAnyPage(driver, url);

        //Печатаем титул страницы в лог
        System.out.println("Page title is: " + driver.getTitle());

        //Вводим корректный логин
        enterField(FieldUser,"tomsmith");

        //Вводим корректный логин
        enterField(FieldPass,"qwerty");

        //Запускаем авторизацию
        pressEnterForElement(FieldPass);

        //Ассерт того что div с результатами отображается
        checkElementDisplayed(searchResult3);

    }


    @AfterMethod
    public void teardown(ITestResult result) {
        makeScreenOnTestFail(result);
        if (driver != null) {
            driver.quit();
        }
    }
}
